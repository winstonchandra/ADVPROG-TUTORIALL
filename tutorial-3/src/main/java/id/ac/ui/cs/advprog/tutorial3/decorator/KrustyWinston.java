package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;

import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class KrustyWinston {

    static int nomorOrder = 0;

    public static void main(String[] args) {
        Food thickBunBurgerSpecial = BreadProducer.THICK_BUN.createBreadToBeFilled();
        thickBunBurgerSpecial = FillingDecorator.BEEF_MEAT.addFillingToBread(
                thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.CHEESE.addFillingToBread(
                thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.CUCUMBER.addFillingToBread(
                thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                thickBunBurgerSpecial);
        printOrder(thickBunBurgerSpecial);

        Food thinBunBurgerVegetarian = BreadProducer.THIN_BUN.createBreadToBeFilled();
        thinBunBurgerVegetarian = FillingDecorator.TOMATO.addFillingToBread(
                thinBunBurgerVegetarian);
        thinBunBurgerVegetarian = FillingDecorator.LETTUCE.addFillingToBread(
                thinBunBurgerVegetarian);
        thinBunBurgerVegetarian = FillingDecorator.CUCUMBER.addFillingToBread(
                thinBunBurgerVegetarian);
        printOrder(thinBunBurgerVegetarian);

        Food beefTomatoSauceSandwich =
                BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
        beefTomatoSauceSandwich =
                FillingDecorator.BEEF_MEAT.addFillingToBread(
                        beefTomatoSauceSandwich);
        beefTomatoSauceSandwich =
                FillingDecorator.TOMATO_SAUCE.addFillingToBread(
                        beefTomatoSauceSandwich);
        printOrder(beefTomatoSauceSandwich);

        Food noCrustFillingSandwich = BreadProducer.NO_CRUST_SANDWICH
                .createBreadToBeFilled();
        noCrustFillingSandwich = FillingDecorator.CHEESE.addFillingToBread(
                noCrustFillingSandwich);
        noCrustFillingSandwich = FillingDecorator.CHICKEN_MEAT.addFillingToBread(
                noCrustFillingSandwich);
        noCrustFillingSandwich = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                noCrustFillingSandwich);
        noCrustFillingSandwich = FillingDecorator.CUCUMBER.addFillingToBread(
                noCrustFillingSandwich);
        noCrustFillingSandwich = FillingDecorator.LETTUCE.addFillingToBread(
                noCrustFillingSandwich);
        printOrder(noCrustFillingSandwich);
    }

    public static void printOrder(Food food) {
        nomorOrder++;
        System.out.println("================= ORDER NUMBER:" + nomorOrder + " =================");
        System.out.println("Preparing your order: ");
        System.out.println(food.getDescription());
        System.out.printf("Here's your order. The total is %.2f\n", food.cost());
        System.out.println();
    }
}
