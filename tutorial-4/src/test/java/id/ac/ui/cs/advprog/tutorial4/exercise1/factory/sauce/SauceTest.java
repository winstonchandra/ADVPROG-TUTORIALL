package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static junit.framework.TestCase.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class SauceTest {

    private Sauce[] testSauce;

    @Before
    public void setUp() {
        testSauce = new Sauce[3];
        testSauce[0] = new WinstonSauce();
        testSauce[1] = new MarinaraSauce();
        testSauce[2] = new PlumTomatoSauce();
    }

    @Test
    public void testAllSauce() {
        assertEquals("Winston Sauce is so yummy", testSauce[0].toString());
        assertEquals("Marinara Sauce", testSauce[1].toString());
        assertEquals("Tomato sauce with plum tomatoes", testSauce[2].toString());
    }

}
