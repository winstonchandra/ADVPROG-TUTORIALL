package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static junit.framework.TestCase.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class DoughTest {

    private Dough[] testDough;

    @Before
    public void setUp() {
        testDough = new Dough[3];
        testDough[0] = new WinstonDough();
        testDough[1] = new ThickCrustDough();
        testDough[2] = new ThinCrustDough();
    }

    @Test
    public void testAllDough() {
        assertEquals("Winston Dough is very best at the moment", testDough[0].toString());
        assertEquals("ThickCrust style extra thick crust dough", testDough[1].toString());
        assertEquals("Thin Crust Dough", testDough[2].toString());
    }

}