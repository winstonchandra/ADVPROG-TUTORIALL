package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static junit.framework.TestCase.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ClamTest {

    private Clams[] testClam;

    @Before
    public void setUp() {
        testClam = new Clams[3];
        testClam[0] = new WinstonClam();
        testClam[1] = new FreshClams();
        testClam[2] = new FrozenClams();
    }

    @Test
    public void testAllClams() {
        assertEquals("Winston Clam tastes very good", testClam[0].toString());
        assertEquals("Fresh Clams from Long Island Sound", testClam[1].toString());
        assertEquals("Frozen Clams from Chesapeake Bay", testClam[2].toString());
    }

}