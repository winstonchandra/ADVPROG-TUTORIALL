package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggie;

import static junit.framework.TestCase.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.WinstonVeggies;

import org.junit.Before;
import org.junit.Test;

public class VeggieTest {

    private Veggies[] testVeggie;

    @Before
    public void setUp() {
        testVeggie = new Veggies[8];
        testVeggie[0] = new BlackOlives();
        testVeggie[1] = new Eggplant();
        testVeggie[2] = new WinstonVeggies();
        testVeggie[3] = new Onion();
        testVeggie[4] = new Garlic();
        testVeggie[5] = new RedPepper();
        testVeggie[6] = new Spinach();
        testVeggie[7] = new Mushroom();
    }

    @Test
    public void testallVeggie() {
        assertEquals("Black Olives", testVeggie[0].toString());
        assertEquals("Eggplant",testVeggie[1].toString());
        assertEquals("Winston Veggies", testVeggie[2].toString());
        assertEquals("Onion",testVeggie[3].toString());
        assertEquals("Garlic",testVeggie[4].toString());
        assertEquals("Red Pepper",testVeggie[5].toString());
        assertEquals("Spinach",testVeggie[6].toString());
        assertEquals("Mushrooms",testVeggie[7].toString());
    }
}
