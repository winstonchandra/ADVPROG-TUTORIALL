package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertNotNull;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {
    private PizzaStore depokPizzaStore;

    @Before
    public void setUp() {
        depokPizzaStore = new DepokPizzaStore();
    }

    @Test
    public void testIfCanMakePizza() {
        Pizza testPizza = depokPizzaStore.orderPizza("cheese");
        assertNotNull(testPizza);
        assertNotNull(testPizza.toString());
        testPizza = depokPizzaStore.orderPizza("veggie");
        assertNotNull(testPizza);
        assertNotNull(testPizza.toString());
        testPizza = depokPizzaStore.orderPizza("clam");
        assertNotNull(testPizza);
        assertNotNull(testPizza.toString());
    }
}
