package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.chesse;

import static junit.framework.TestCase.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.WinstonCheese;

import org.junit.Before;
import org.junit.Test;

public class CheeseTest {

    private Cheese[] testCheese;

    @Before
    public void setUp() {
        testCheese = new Cheese[4];
        testCheese[0] = new ParmesanCheese();
        testCheese[1] = new WinstonCheese();
        testCheese[2] = new MozzarellaCheese();
        testCheese[3] = new ReggianoCheese();
    }

    @Test
    public void testAllCheese() {
        assertEquals("Shredded Parmesan", testCheese[0].toString());
        assertEquals("Winston Cheese is the best", testCheese[1].toString());
        assertEquals("Shredded Mozzarella", testCheese[2].toString());
        assertEquals("Reggiano Cheese", testCheese[3].toString());
    }

}
