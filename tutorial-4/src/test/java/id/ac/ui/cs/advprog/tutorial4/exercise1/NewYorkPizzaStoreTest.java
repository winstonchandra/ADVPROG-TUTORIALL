package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertNotNull;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaStoreTest {
    private PizzaStore newYorkPizzaStore;

    @Before
    public void setUp() {
        newYorkPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void testCanMakePizza() {
        Pizza testPizza = newYorkPizzaStore.orderPizza("cheese");
        assertNotNull(testPizza);
        testPizza = newYorkPizzaStore.orderPizza("veggie");
        assertNotNull(testPizza);
        testPizza = newYorkPizzaStore.orderPizza("clam");
        assertNotNull(testPizza);
    }
}
