package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class WinstonDough implements Dough {

    public String toString() {
        return "Winston Dough is very best at the moment";
    }
}
