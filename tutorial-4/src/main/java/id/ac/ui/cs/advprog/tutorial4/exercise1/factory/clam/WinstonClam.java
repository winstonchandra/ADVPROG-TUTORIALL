package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class WinstonClam implements Clams {

    public String toString() {
        return "Winston Clam tastes very good";
    }
}
