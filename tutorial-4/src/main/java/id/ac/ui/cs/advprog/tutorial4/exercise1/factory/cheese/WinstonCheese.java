package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class WinstonCheese implements Cheese {

    public String toString() {
        return "Winston Cheese is the best";
    }
}
