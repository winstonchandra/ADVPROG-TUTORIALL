package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class WinstonSauce implements Sauce {

    public String toString() {
        return "Winston Sauce is so yummy";
    }
}
